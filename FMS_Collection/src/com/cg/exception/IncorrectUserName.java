package com.cg.exception;

public class IncorrectUserName extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		String message = "\nxxxxxxxxxxxxxxxxxxxxxxxxxx\nThe UserName you have Entered is Invalid or not present"
				+ "\nPlease enter correct the UserName and try again\nxxxxxxxxxxxxxxxxxxxxxxxxxx";
		return message;
	}

}
