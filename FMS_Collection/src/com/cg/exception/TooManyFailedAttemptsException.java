package com.cg.exception;

public class TooManyFailedAttemptsException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		String message = "\nxxxxxxxxxxxxxxxxxxxxxxxxxx\nToo Many Failed Attempts!!!"
				+ "\nxxxxxxxxxxxxxxxxxxxxxxxxxx";
		return message;
	}

} 