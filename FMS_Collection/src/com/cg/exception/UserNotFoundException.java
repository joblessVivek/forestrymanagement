package com.cg.exception;

public class UserNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		String message = "\nxxxxxxxxxxxxxxxxxxxxxxxxxx\nThe UserName you have Entered is Invalid or not present"
				+ "\nxxxxxxxxxxxxxxxxxxxxxxxxxx";
		return message;
	}

}