package com.cg.exception;

public class IncorrectPasswordException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		String message = "\nxxxxxxxxxxxxxxxxxxxxxxxxxx\nThe Password you have Entered is Incorrect"
				+ "\nPlease enter correct password and try again\nxxxxxxxxxxxxxxxxxxxxxxxxxx";
		return message;
	}
}
