package com.cg.services;

public interface HaulierService {

	// Schedule Delivery
	public Boolean scheduleTransport(Integer contractid , Integer extradays);

	// Update delivery date
	public void updateDeliveryDate();
}
