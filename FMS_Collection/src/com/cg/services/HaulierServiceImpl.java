package com.cg.services;

import com.cg.dao.HaulierDAO;
import com.cg.dao.HaulierDAOImpl;

public class HaulierServiceImpl implements HaulierService {
	HaulierDAO hdao = new HaulierDAOImpl();

	@Override
	public Boolean scheduleTransport(Integer contractid , Integer extradays) {
		return hdao.scheduleTransport(contractid, extradays);
	}

	@Override
	public void updateDeliveryDate() {

	}

}
