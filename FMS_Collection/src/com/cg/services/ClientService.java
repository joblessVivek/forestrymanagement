package com.cg.services;

import com.cg.beans.Client;
import com.cg.beans.Contract;

public interface ClientService {
	
	//Client Login
	public Client clientLogin(Integer cliendid,String password);
	
	// Update Password
	public Boolean updatePassword(Integer cliendid,String oldpassword, String newPassword);

	// Update Email
	public Boolean updateEmail(Integer cliendid,String email);

	// Update Address
	public Boolean updateAddress(Integer cliendid,String address);

	// Update Phone no.
	public Boolean updatePhoneno(Integer cliendid,String newno);

	// Place new Contract
	public Contract createNewContract(Integer cliendid,Contract contract);

	// Modify contract quantity
	public Contract modifyContractQuantity(Integer cliendid,Integer contractid, Integer newquantity, String password);

}
