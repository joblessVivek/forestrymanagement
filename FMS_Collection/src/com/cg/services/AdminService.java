package com.cg.services;

import java.util.List;

import com.cg.beans.Client;
import com.cg.beans.Contract;

public interface AdminService {

	// Create Client
	public Client createClientProfile(Client client);

	// Search Client
	public Client searchClient(Integer id);

	// Delete Client
	public Client deleteClient(Integer id);

	// Get all Clients
	public List<Client> getAllClients();

	// Get all Contracts
	public List<Contract> getAllContracts();

}
