package com.cg.services;

import java.util.List;

import com.cg.beans.Haulier;
import com.cg.beans.Inventory;

public interface SchedulerService {

	// Create new entry in inventory
	public Boolean createInventoryProfile(Inventory inv);

	// Search for inventory
	public Inventory searchInventory(String invid);

	// Delete Inventory
	public Inventory deleteInventory(String invid);

	// Update inventory
	public Inventory updateInventory(String invid, Integer newquantity);

	// Get all students
	public List<Inventory> getAllInventory();

	// Schedule delivery
	public Haulier scheduleDelivery(String doo, String eta);

}
