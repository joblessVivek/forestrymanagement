package com.cg.services;

import com.cg.beans.Client;
import com.cg.beans.Contract;
import com.cg.dao.ClientDAO;
import com.cg.dao.ClientDAOImpl;

public class ClientServiceImpl implements ClientService {

	ClientDAO cdao = new ClientDAOImpl();

	@Override
	public Client clientLogin(Integer cliendid, String password) {
		return cdao.clientLogin(cliendid, password);
	}
	
	@Override
	public Boolean updatePassword(Integer cliendid,String oldpassword, String newpassword) {
		return cdao.updatePassword(cliendid, oldpassword, newpassword);
	}

	@Override
	public Boolean updateEmail(Integer cliendid,String email) {
		return cdao.updateEmail(cliendid, email);
	}

	@Override
	public Boolean updateAddress(Integer cliendid,String address) {
		return cdao.updateAddress(cliendid,address);
	}

	@Override
	public Boolean updatePhoneno(Integer cliendid,String newno) {
		return cdao.updatePhoneno(cliendid,newno);
	}

	@Override
	public Contract createNewContract(Integer cliendid,Contract contract) {
		return cdao.createNewContract(cliendid,contract);
	}

	@Override
	public Contract modifyContractQuantity(Integer cliendid,Integer contractid, Integer newquantity, String password) {
		return cdao.modifyContractQuantity(cliendid,contractid, newquantity, password);
	}

}
