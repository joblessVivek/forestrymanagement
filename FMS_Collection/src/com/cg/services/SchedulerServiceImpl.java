package com.cg.services;

import java.util.List;

import com.cg.beans.Haulier;
import com.cg.beans.Inventory;
import com.cg.dao.SchedulerDAO;
import com.cg.dao.SchedulerDAOImpl;

public class SchedulerServiceImpl implements SchedulerService{

	SchedulerDAO sdao=new SchedulerDAOImpl();
	
	@Override
	public Boolean createInventoryProfile(Inventory inv) {
		return sdao.createInventoryProfile(inv);
	}

	@Override
	public Inventory searchInventory(String invid) {
		return sdao.searchInventory(invid);
	}

	@Override
	public Inventory deleteInventory(String invid) {
		return sdao.deleteInventory(invid);
	}

	@Override
	public Inventory updateInventory(String invid,Integer newquantity) {
		return sdao.updateInventory(invid,newquantity);
	}

	@Override
	public List<Inventory> getAllInventory() {
		return sdao.getAllInventory();
	}

	@Override
	public Haulier scheduleDelivery(String doo, String eta) {
		return sdao.scheduleDelivery(doo, eta);
	}

}
