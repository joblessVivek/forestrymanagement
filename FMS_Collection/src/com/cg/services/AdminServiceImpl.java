package com.cg.services;

import java.util.List;

import com.cg.beans.Client;
import com.cg.beans.Contract;
import com.cg.dao.AdminDAO;
import com.cg.dao.AdminDAOImpl;

public class AdminServiceImpl implements AdminService{

	AdminDAO adao=new AdminDAOImpl();
	
	@Override
	public Client createClientProfile(Client client) {
		return adao.createClientProfile(client);
	}

	@Override
	public Client searchClient(Integer id) {
		return adao.searchClient(id);
	}

	@Override
	public Client deleteClient(Integer id) {
		return adao.deleteClient(id);
	}

	@Override
	public List<Client> getAllClients() {
		return adao.getAllClients();
	}

	@Override
	public List<Contract> getAllContracts() {
		return adao.getAllContracts();
	}

	
}
