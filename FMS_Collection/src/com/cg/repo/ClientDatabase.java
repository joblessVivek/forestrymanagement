package com.cg.repo;

import java.util.HashMap;

import java.util.Map;

import com.cg.beans.Client;
import com.cg.beans.Contract;

public class ClientDatabase {

	public Map<Integer, Client> map;

	ClientContract cc=new ClientContract();
    public Contract con=new Contract();
	public ClientDatabase() {

		 map = new HashMap<Integer, Client>();
		// Creating new client
		Client c1 = new Client();
		c1.setClientid(1);
		c1.setClientname("Zaid");
		c1.setClientadd("BhavaniPeth,Pune");
		c1.setClientpassword("Zaid");
		// Creating new contract for Clientid1
		Contract con1=con;
		con1 = new Contract();
		con1.setContractid(15081248);
		con1.setRequiredproductid("wood2");
		con1.setRequiredquantity(2580);
	    cc.map4contract.put(con1.getContractid(), con1);
		c1.setContract(con1);
	    map.put(c1.getClientid(), c1);
	     
		Client c2 = new Client();
		c2.setClientid(2);
		c2.setClientname("Vikas");
		c2.setClientadd("Kalewadi,Pune");
		c2.setClientpassword("Vikas");
		Contract con2=con;
		c2.setContract(con2);
		map.put(c2.getClientid(), c2);

		Client c3 = new Client();
		c3.setClientid(3);
		c3.setClientname("Tejas");
		c3.setClientadd("Sangli");
		c3.setClientpassword("Tejas");
		map.put(c3.getClientid(), c3);

		Client c4 = new Client();
		c4.setClientid(4);
		c4.setClientname("Keyur");
		c4.setClientadd("Singhgad,Pune");
		c4.setClientpassword("Keyur");
		map.put(c4.getClientid(), c4);

		Client c5 = new Client();
		c5.setClientid(5);
		c5.setClientname("Bharat");
		c5.setClientadd("Pimpri");
		c5.setClientpassword("Bharat");
		map.put(c5.getClientid(), c5);
	}

}
