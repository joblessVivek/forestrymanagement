package com.cg.repo;

import java.util.HashMap;
import java.util.Map;

import com.cg.beans.Inventory;

public class InventoryDatabase {

	public Map<String, Inventory> map;

	public InventoryDatabase() {

		map = new HashMap<String, Inventory>();

		Inventory i1 = new Inventory();
		i1.setProductid("wood1");
		i1.setProductname("SoftWood");
		i1.setProductdesc("This type of wood is light, soft and easy to" + " work with when compared to hardwoods");
		i1.setAvailableproductquantity(100000);
		map.put(i1.getProductid(), i1);

		Inventory i2 = new Inventory();
		i2.setProductid("wood2");
		i2.setProductname("HarsedWood");
		i2.setProductdesc("The woods are strong and more durable than softwood,"
				+ " they are used for flooring, doors and windows and heavy furniture");
		i2.setAvailableproductquantity(20000);
		map.put(i2.getProductid(), i2);

		Inventory i3 = new Inventory();
		i3.setProductid("fruit1");
		i3.setProductname("Apple");
		i3.setProductdesc("It is red or green");
		i3.setAvailableproductquantity(568);
		map.put(i3.getProductid(), i3);

		Inventory i4 = new Inventory();
		i4.setProductid("fruit2");
		i4.setProductname("Grapes");
		i4.setProductdesc("It is green");
		i4.setAvailableproductquantity(5987);
		map.put(i4.getProductid(), i4);

		Inventory i5 = new Inventory();
		i5.setProductid("vegetable1");
		i5.setProductname("Potato");
		i5.setProductdesc("Full of Protiens");
		i5.setAvailableproductquantity(59887);
		map.put(i5.getProductid(), i5);
	}
}
