package com.cg.app;

import java.util.Date;
import java.util.Scanner;

import com.cg.beans.Client;
import com.cg.beans.Contract;
import com.cg.dao.AdminDAOImpl;
import com.cg.dao.SchedulerDAOImpl;
import com.cg.services.ClientService;
import com.cg.services.ClientServiceImpl;

public class App4client {

	public static void client() {
		Scanner sc = new Scanner(System.in);
		ClientService clientservice = new ClientServiceImpl();
		
		outter: while (true) {
			System.out.println("Welcome Client!!!");
			System.out.println("Please enter your login credentiials:");
			System.out.println("Enter ClientId:");
			Integer clientid = Integer.parseInt(sc.nextLine());
			System.out.println("Enter your Password");
			String passwd = sc.nextLine();
			Client loggedclient = clientservice.clientLogin(clientid, passwd);
			if (loggedclient == null) {
				System.out.println("Couldn't find user");
			} else {
				System.out.println("Login Successful!!!");
			}
			Integer loggedclientid = loggedclient.getClientid();

			while (true) {
				System.out.println("Enter 1 to create new Contract" + "\nEnter 2 to modify the contract"
						+ "\nEnter 3 to update your Password" + "\nEnter 4 to update your Email address"
						+ "\nEnter 5 to update your Phone no." + "\nEnter 6 to update Address"
						+ "\nEnter 7 to get exit Client Module");
				Integer clientmodulechoice = null;
				clientmodulechoice = Integer.parseInt(sc.nextLine());

				switch (clientmodulechoice) { 

				case 1: {
					System.out.println("Enter new ContractId:");
					Integer conid = Integer.parseInt(sc.nextLine());
					Contract cnew = new Contract();
					cnew.setContractid(conid);
					System.out.println("Enter required item's productid:");
					String reqitemid = sc.nextLine();
					cnew.setRequiredproductid(reqitemid);
					System.out.println("Enter the amount of required quaninty:");
					Integer reqitemquant = Integer.parseInt(sc.nextLine());
					cnew.setRequiredquantity(reqitemquant);

					Integer availablequantity = SchedulerDAOImpl.i.map.get(reqitemid).getAvailableproductquantity();

					if (availablequantity > cnew.getRequiredquantity()) {
						clientservice.createNewContract(loggedclientid, cnew);
						System.out.println("The contract with following info:" + cnew
								+ "\nhas been placed successfully at " + new Date());
						System.out.println("Please contact the haulier for further info...");
						SchedulerDAOImpl.i.map.get(reqitemid).setAvailableproductquantity(availablequantity - cnew.getRequiredquantity());
						
					} else {
						System.out.println("Sorry the required quantity is not avavilable...");
						System.out.println("The avavilable quanitiy of the required product is:" + availablequantity);
					}
					break;
				}
				case 2: {
					System.out.println("Enter the ContractId you want to modify:");
					Integer id4search = Integer.parseInt(sc.nextLine());
					Contract con4update = AdminDAOImpl.cc.map4contract.get(id4search);
					
					Contract c = loggedclient.getContract();
					String conid = c.getRequiredproductid();
		
					System.out.println("Enter the updated quantity:");
					Integer updatedquant = Integer.parseInt(sc.nextLine());
					c.setRequiredquantity(updatedquant);
					
					Integer availablequantity = SchedulerDAOImpl.i.map.get(conid).getAvailableproductquantity();

					if (updatedquant > availablequantity) {
						System.out.println("Sorry the required amount is not available...");
						System.out.println("The avavilable quanitiy of the required product is:" + availablequantity);

					} else {
						SchedulerDAOImpl.i.map.get(conid).setAvailableproductquantity(updatedquant - availablequantity);
						con4update.setRequiredquantity(updatedquant);
						System.out.println("The quantity has been added successfully!!!");
					}
					break;
				}

				case 3: {
					System.out.println("Enter oldpassword:");
					String oldpassword = sc.nextLine();
					System.out.println("Enter new password:");
					String newpassword = sc.nextLine();
					Boolean updated = clientservice.updatePassword(loggedclientid, oldpassword, newpassword);
					if (updated) {
						System.out.println("Updation Successful!!");
						System.out.println("From now on your password is:" + newpassword);
					} else {
						System.out.println("Updation failed");
					}
					break;
				}

				case 4: {
					System.out.println("Enter new email:");
					String updatedemail = sc.nextLine();
					Boolean clientupdated = clientservice.updateEmail(loggedclientid, updatedemail);
					if (clientupdated) {
						System.out.println("Updation Successful!!");
						System.out.println("From now on your email is:" + updatedemail);
					} else {
						System.out.println("Updation failed");
					}

					break;
				}

				case 5: {
					System.out.println("Enter your new phoneno.:");
					String updatedno = sc.nextLine();
					Boolean clientupdate = clientservice.updatePhoneno(loggedclientid, updatedno);
					if (clientupdate) {
						System.out.println("Updation Successful!!");
						System.out.println("From now on your phone no. is:" + updatedno);
					} else {
						System.out.println("Updation failed");
					}

					break;
				}

				case 6: {
					System.out.println("Enter your new addresss:");
					String updatedadd = sc.nextLine();
					Boolean clientupdate = clientservice.updateAddress(loggedclientid, updatedadd);
					if (clientupdate) {
						System.out.println("Updation Successful!!");
						System.out.println("From now on your address no. is:" + updatedadd);
					} else {
						System.out.println("Updation failed");
					}
					break;
				}

				case 7: {
					App.mainApp();
					sc.close();
					break outter;
				}
				default:{
					System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
					System.out.println("Please enter valid option!!");
					System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
					break;
				}
				}

			}

		}

	}
}