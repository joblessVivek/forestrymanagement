package com.cg.app;

import java.util.List;
import java.util.Scanner;

import com.cg.beans.Client;
import com.cg.beans.Contract;
import com.cg.services.AdminService;
import com.cg.services.AdminServiceImpl;

public class App4admin {

	public static void admin() {
		Scanner sc = new Scanner(System.in);
		Client newClient = new Client();
		AdminService adminservice = new AdminServiceImpl();
		List<Client> ls = null;
		List<Contract> ls4contract = null;
		outter: while (true) {
			System.out.println("Enter 1 to create new Client Profile" + "\nEnter 2 to search a Client"
					+ "\nEnter 3 to delete a Client" + "\nEnter 4 to get information of all Clients"
					+ "\nEnter 5 to get information of all Contracts" + "\nEnter 6 to get exit Admin Module");
			Integer adminmodulechoice = null;
			adminmodulechoice = Integer.parseInt(sc.nextLine());

			switch (adminmodulechoice) {

			case 1: {
				System.out.println("Enter ClientId:");
				Integer clientid = Integer.parseInt(sc.nextLine());
				newClient.setClientid(clientid);
				System.out.println("Enter name of Client:");
				String clientname = sc.nextLine();
				newClient.setClientname(clientname);
				System.out.println("Enter email address of Client:");
				String clientemail = sc.nextLine();
				newClient.setClientemail(clientemail);
				System.out.println("Enter Contact No. of Client:");
				String clientfno = sc.nextLine();
				newClient.setClientphoneno(clientfno);
				;
				System.out.println("Enter address of Client:");
				String clientaddress = sc.nextLine();
				newClient.setClientadd(clientaddress);
				System.out.println("Enter password for Client:");
				String clientpassword = sc.nextLine();
				newClient.setClientpassword(clientpassword);
				if (adminservice.createClientProfile(newClient) == null) {
				} else {
					System.out.println("Client having following info has been added successfully: " + newClient);
					System.out.println("You can give the client following login credentials:");
					System.out.println("ClientId:" + newClient.getClientid());
					System.out.println("Password:" + newClient.getClientpassword());
				}
				break;
			}
			case 2: {
				System.out.println("Enter ClientId of client you want to search:");
				Integer id4search = Integer.parseInt(sc.nextLine());
				System.out.println(
						"The student you are looking for has following info:" + adminservice.searchClient(id4search));
				break;
			}

			case 3: {
				System.out.println("Enter ClientId of client you want to delete:");
				Integer id4delete = Integer.parseInt(sc.nextLine());
				System.out.println(
						"The student with following details has been deleted:" + adminservice.deleteClient(id4delete));
				break;
			}

			case 4: {
				System.out.println("Following is information of all the clients:");
				ls = adminservice.getAllClients();
				System.out.println(ls);
				break;
			}
			case 5: {
				System.out.println("Following is information of all the contracts:");
				ls4contract = adminservice.getAllContracts();
				System.out.println(ls4contract);
				break;
			}
			case 6: {
				App.mainApp();
				sc.close();
				break outter;
			}
			default:{
				System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
				System.out.println("Please enter valid option!!");
				System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
				break ;
			}

			}
		}

	}
}
