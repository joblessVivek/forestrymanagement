package com.cg.app;

import java.util.Scanner;

import com.cg.exception.IncorrectPasswordException;
import com.cg.exception.IncorrectUserName;
import com.cg.exception.TooManyFailedAttemptsException;

public class App {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		App.mainApp();
	}

	public static void mainApp() {

		System.out.println("=======================================");
		System.out.println("   WELCOME TO FMS_COLLECTION   ");
		System.out.println("=======================================");
		
		while (true) {
			System.out.println("Please select one of the following option:");
			System.out.println("Enter 1 for Admin Module");
			System.out.println("Enter 2 for Client Module");
			System.out.println("Enter 3 for Scheduler Module");
			System.out.println("Enter 4 for Haulier Module");
			Integer mainchoice = Integer.parseInt(sc.nextLine());
			outer: switch (mainchoice) {
			case 1: {
				for (int i = 0; i <= 3; i++) {
					System.out.println("Please Enter your User Name:");
					String adminlogin = sc.nextLine();
					if (adminlogin.equals("admin")) {
						for (int j = 0; j <= 3; j++) {
							System.out.println("Please Enter your password:");
							String adminpass = sc.nextLine();
							if (adminpass.equals("admin")) {
								App4admin.admin();
							} else {
								if (j == 3) {
									throw new TooManyFailedAttemptsException();
								} else {
									if (j == 2) {
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										System.out.println("This is your last chance");
										System.out.println(
												"Enter correct password\nElse the application will be terminated");
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
									} else {
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										System.out.println("Incorrect Password!!");
										System.out.println("Try again...");
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										continue;
									}
								}
							}
						}
					} else {
						if (i == 3) {
							throw new TooManyFailedAttemptsException();
						} else {
							if (i == 2) {
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								System.out.println("This is your last chance");
								System.out.println("Enter correct username\nElse the application will be terminated");
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
							} else {
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								System.out.println("Incorrect UserName!!");
								System.out.println("Try again...");
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								continue;
							}
						}
					}
				}
				break;

			}
			case 2: {
				App4client.client();
			}
			case 3: {
				for (int i = 0; i <= 3; i++) {
					System.out.println("Please Enter your User Name:");
					String schlogin = sc.nextLine();
					if (schlogin.equals("scheduler")) {
						for (int j = 0; j <= 3; j++) {
							System.out.println("Please Enter your password:");
							String schpass = sc.nextLine();
							if (schpass.equals("scheduler")) {
								App4Scheduler.scheduler();
							} else {
								if (j == 3) {
									throw new IncorrectPasswordException();
								} else {
									if (j == 2) {
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										System.out.println("This is your last chance");
										System.out.println(
												"Enter correct password\nElse the application will be terminated");
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
									} else {
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										System.out.println("Incorrect Password!!");
										System.out.println("Try again...");
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										continue;
									}
								}
							}
						}
					} else {
						if (i == 3) {
							throw new TooManyFailedAttemptsException();
						} else {
							if (i == 2) {
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								System.out.println("This is your last chance");
								System.out.println("Enter correct username\nElse the application will be terminated");
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
							} else {
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								System.out.println("Incorrect UserName!!");
								System.out.println("Try again...");
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								continue;
							}
						}
					}
				}
			}

			case 4:

			{

				for (int i = 0; i <= 3; i++) {
					System.out.println("Please Enter your User Name:");
					String haullogin = sc.nextLine();
					if (haullogin.equals("haulier")) {
						for (int j = 0; j <= 3; j++) {
							System.out.println("Please Enter your password:");
							String haulpass = sc.nextLine();
							if (haulpass.equals("haulier")) {
								App4Haulier.haulier();
							} else {
								if (j == 3) {
									throw new TooManyFailedAttemptsException();
								} else {
									if (j == 2) {
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										System.out.println("This is your last chance");
										System.out.println(
												"Enter correct password\nElse the application will be terminated");
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
									} else {
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										System.out.println("Incorrect Password!!");
										System.out.println("Try again...");
										System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
										continue;
									}
								}
							}
						}
					} else {
						if (i == 3) {
							throw new IncorrectUserName();
						} else {
							if (i == 2) {
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								System.out.println("This is your last chance");
								System.out.println("Enter correct username\nElse the application will be terminated");
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
							} else {
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								System.out.println("Incorrect UserName!!");
								System.out.println("Try again...");
								System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
								continue;
							}
						}
					}
				}
			}

			case 5: {
				System.out.println("Please enter valid option");
				break outer;
			}
			default: {
				System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
				System.out.println("Please enter valid option!!");
				System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
				break outer;
			}
			}

		}

	}

}
