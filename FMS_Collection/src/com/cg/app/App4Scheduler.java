package com.cg.app;

import java.util.List;
import java.util.Scanner;

import com.cg.beans.Inventory;
import com.cg.services.SchedulerService;
import com.cg.services.SchedulerServiceImpl;

public class App4Scheduler {

	public static void scheduler() {
		Scanner sc = new Scanner(System.in);
		
		SchedulerService schservice = new SchedulerServiceImpl();
		List<Inventory> ls = null;
		outter: while (true) {
			System.out.println("Enter 1 to create new Product Profile" + "\nEnter 2 to search for a Product"
					+ "\nEnter 3 to delete a Product from inventory"
					+ "\nEnter 4 to update available quantity of a product"
					+ "\nEnter 5 to get information of all the products present in the inventory "
					+ "\nEnter 6 to get exit Scheduler Module");
			Integer schedulemodulechoice = null;
			schedulemodulechoice = Integer.parseInt(sc.nextLine());

			switch (schedulemodulechoice) {

			case 1: {
				System.out.println("Enter new InventoryId:");
				String invid = sc.nextLine();
				Inventory newInventory = new Inventory();
				newInventory.setProductid(invid);

				System.out.println("Enter name of item you want to add to the inventory:");
				String invname = sc.nextLine();
				newInventory.setProductname(invname);

				System.out.println("Enter description of the product:");
				String invdesc = sc.nextLine();
				newInventory.setProductdesc(invdesc);

				System.out.println("Enter the available amount of the product:");
				Integer invav = Integer.parseInt(sc.nextLine());
				newInventory.setAvailableproductquantity(invav);
				
				
				if (schservice.createInventoryProfile(newInventory) == false) {
					System.out.println("Inventoey not created...");
				} else {
					
					System.out.println("Product having following info has been added successfully to the Inventory: "
							+ newInventory);
				}
				break;
			}
			case 2: {
				System.out.println("Enter InventoryId of inventory you want to search:");
				String id4search = sc.nextLine();
				System.out.println(
						"The product you are looking for has following info:" + schservice.searchInventory(id4search));
				break;

			}
			case 3: {
				System.out.println("Enter InventoryId of product you want to delete:");
				String id4del = sc.nextLine();
				System.out.println(
						"The product with following details has been deleted:" + schservice.deleteInventory(id4del));
				break;
			}

			case 4: {
				System.out.println("Enter InventoryId of product you want to update:");
				String id4update = sc.nextLine();
				System.out.println("Enter updated quantity of the product:");
				Integer newquant = Integer.parseInt(sc.nextLine());
				System.out.println("The product with updated details is as follows:"
						+ schservice.updateInventory(id4update, newquant));
				break;
			}

			case 5: {
				System.out.println("Following is information of all the products present in the inventory:");
				ls = schservice.getAllInventory();
				System.out.println(ls);
				break;
			}
			case 6: {
				App.mainApp();
				sc.close();
				break outter;
			}
			default:{
				System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
				System.out.println("Please enter valid option!!");
				System.out.println("xxxxxxxxxxxxxxxxxxxxxx");
				break ;
			}
			}

		}

	}

}
