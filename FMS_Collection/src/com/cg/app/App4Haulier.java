package com.cg.app;

import java.util.List;
import java.util.Scanner;

import com.cg.beans.Contract;
import com.cg.services.AdminService;
import com.cg.services.AdminServiceImpl;
import com.cg.services.HaulierService;
import com.cg.services.HaulierServiceImpl;

public class App4Haulier {

	static List<Contract> list4contract = null;
	static {
		AdminService adminservice = new AdminServiceImpl();
		list4contract = adminservice.getAllContracts();
	}
	static List<Contract> listcopy = list4contract;
	public static void haulier() {
		Scanner sc = new Scanner(System.in);
		HaulierService haulservice = new HaulierServiceImpl();
		System.out.println("Following is the list of new Contracts approved..."
				+ "\nPlease enter any 1 contract id to set delivery date");
		System.out.println(listcopy);
		Integer conid = Integer.parseInt(sc.nextLine());
		System.out.println("Enter number of days required to deliver the selected contract:");
		Integer extradays = Integer.parseInt(sc.nextLine());
		haulservice.scheduleTransport(conid, extradays);
		listcopy.remove((Object) conid);
		App.mainApp();
		sc.close();
	}

}
