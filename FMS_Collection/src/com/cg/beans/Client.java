package com.cg.beans;

public class Client {
	private Integer clientid;
	private String clientname;
	private String clientemail;
	private String clientphoneno;
	private String clientadd;
	private String clientpassword;
	private Contract contract;

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getClientemail() {
		return clientemail;
	}

	public void setClientemail(String clientemail) {
		this.clientemail = clientemail;
	}

	public String getClientpassword() {
		return clientpassword;
	}

	public void setClientpassword(String clientpassword) {
		this.clientpassword = clientpassword;
	}

	public Integer getClientid() {
		return clientid;
	}

	public void setClientid(Integer clientid) {
		this.clientid = clientid;
	}

	public String getClientname() {
		return clientname;
	}

	public void setClientname(String clientname) {
		this.clientname = clientname;
	}

	public String getClientadd() {
		return clientadd;
	}

	public void setClientadd(String clientadd) {
		this.clientadd = clientadd;
	}

	public String getClientphoneno() {
		return clientphoneno;
	}

	public void setClientphoneno(String clientphoneno) {
		this.clientphoneno = clientphoneno;
	}

	@Override
	public String toString() {
		return "\nClientId=" + clientid + "\nName of the client is " + clientname + "\nClient's emailaddress="
				+ clientemail + "\nDelivery address of client is " + clientadd + "\nClient's Phone No.:" + clientphoneno
				+ "\n=========================";
	}
}
