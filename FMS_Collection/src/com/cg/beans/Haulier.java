package com.cg.beans;

public class Haulier {

	private String tid;
	private String productid;
	private Integer clientid;
	private String clientaddress;
	private String orderplacedondate;
	private String expecteddeliverydate;

	public String getTid() {
		return tid;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public Integer getClientid() {
		return clientid;
	}

	public void setClientid(Integer clientid) {
		this.clientid = clientid;
	}

	public String getClientaddress() {
		return clientaddress;
	}

	public void setClientaddress(String clientaddress) {
		this.clientaddress = clientaddress;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getOrderplacedondate() {
		return orderplacedondate;
	}

	public void setOrderplacedondate(String orderplacedondate) {
		this.orderplacedondate = orderplacedondate;
	}

	public String getExpecteddeliverydate() {
		return expecteddeliverydate;
	}

	public void setExpecteddeliverydate(String expecteddeliverydate) {
		this.expecteddeliverydate = expecteddeliverydate;
	}

	@Override
	public String toString() {
		return "Haulier [tid=" + tid + ", productid=" + productid + ", clientid=" + clientid + ", clientaddress="
				+ clientaddress + ", orderplacedondate=" + orderplacedondate + ", expecteddeliverydate="
				+ expecteddeliverydate + "]";
	}

}
