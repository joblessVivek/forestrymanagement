package com.cg.beans;

public class Inventory {

	private String productid;
	private String productname;
	private String productdesc;
	private Integer availableproductquantity;

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getProductdesc() {
		return productdesc;
	}

	public void setProductdesc(String productdesc) {
		this.productdesc = productdesc;
	}

	public Integer getAvailableproductquantity() {
		return availableproductquantity;
	}

	public void setAvailableproductquantity(Integer newquantity) {
		this.availableproductquantity = newquantity;
	}

	@Override
	public String toString() {
		return "\nProductId=" + productid + "\nProduct Name=" + productname + "\nProduct Description=" + productdesc
				+ "\nAvailable Quantity of Product=" + availableproductquantity + "\n======================";
	}
}
