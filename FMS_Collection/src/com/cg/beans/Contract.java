package com.cg.beans;

public class Contract {
	private Integer contractid;
	private String requiredproductid;
	private Integer requiredquantity;

	public Integer getContractid() {
		return contractid;
	}

	public void setContractid(Integer contractid) {
		this.contractid = contractid;
	}

	public String getRequiredproductid() {
		return requiredproductid;
	}

	public void setRequiredproductid(String requiredproductid) {
		this.requiredproductid = requiredproductid;
	}

	public Integer getRequiredquantity() {
		return requiredquantity;
	}

	public void setRequiredquantity(Integer requiredquantity) {
		this.requiredquantity = requiredquantity;
	}

	@Override
	public String toString() {
		return "\nContractid=" + contractid + "\nRequired Productid=" + requiredproductid + "\nRequired Quantity="
				+ requiredquantity + "\n*-*-*-*-*-*-*-*-*-*-*-*-*";
	}
}