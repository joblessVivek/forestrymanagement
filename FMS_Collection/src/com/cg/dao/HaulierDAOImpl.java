package com.cg.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.cg.beans.Haulier;

public class HaulierDAOImpl implements HaulierDAO {

	@Override
	public Boolean scheduleTransport(Integer contractid, Integer extradays) {
		Haulier transcon = ClientDAOImpl.haulcontract.map4haul.get("Transport4contractid:" + contractid);
		System.out.println("Following is contract summary:");
		System.out.println(transcon);
		String olddate = transcon.getOrderplacedondate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(olddate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.add(Calendar.DAY_OF_MONTH, +extradays);
		String newDate = sdf.format(c.getTime());
		transcon.setExpecteddeliverydate(newDate);
		System.out.println("You have to deliver the given contract within the date:" + newDate);
		return true;
	}

	@Override
	public void updateDeliveryDate() {

	}

}
