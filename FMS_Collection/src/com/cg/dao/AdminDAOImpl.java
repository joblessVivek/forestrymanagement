package com.cg.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import com.cg.beans.Client;
import com.cg.beans.Contract;
import com.cg.repo.ClientContract;
import com.cg.repo.ClientDatabase;

public class AdminDAOImpl implements AdminDAO {
	static Scanner sc = new Scanner(System.in);
	public static ClientContract cc = new ClientContract();
	public static ClientDatabase db = new ClientDatabase();

	@Override
	public Client createClientProfile(Client client) {
		db.map.put(client.getClientid(), client);
		return client;
	}

	@Override
	public Client searchClient(Integer id) {
		if (db.map.containsKey(id)) {
			return db.map.get(id);
		} else {
			return null;
		}
	}

	@Override
	public Client deleteClient(Integer id) {
		if (db.map.containsKey(id)) {
			return db.map.remove(id);
		} else {
			return null;
		}
	}

	@Override
	public List<Client> getAllClients() {
		Collection<Client> c = db.map.values();
		List<Client> al = new ArrayList<Client>(c);
		return al;
	}

	@Override
	public List<Contract> getAllContracts() {
		Collection<Contract> c = cc.map4contract.values();
		List<Contract> al = new ArrayList<Contract>(c);
		return al;
	}
}