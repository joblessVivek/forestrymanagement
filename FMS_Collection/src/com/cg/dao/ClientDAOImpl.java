package com.cg.dao;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.cg.beans.Client;
import com.cg.beans.Contract;
import com.cg.beans.Haulier;
import com.cg.exception.UserNotFoundException;
import com.cg.repo.ClientContract;
import com.cg.repo.HaulierContracts;

public class ClientDAOImpl implements ClientDAO {
	ClientContract cc = new ClientContract();
	public static HaulierContracts haulcontract =new HaulierContracts();
	@Override
	public Client clientLogin(Integer cliendid, String password) {
		Client clogged = AdminDAOImpl.db.map.get(cliendid);
		if(clogged==null)
		{
			throw new UserNotFoundException();
		}
		if (clogged.getClientpassword().equals(password)) {
			return clogged;
		} else {
			return null;
		}
	}

	@Override
	public Boolean updatePassword(Integer cliendid, String oldpassword, String newpassword) {
		Client c4updatep = AdminDAOImpl.db.map.get(cliendid);
		if (c4updatep.getClientpassword().equals(oldpassword)) {
			c4updatep.setClientpassword(newpassword);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Boolean updateEmail(Integer cliendid, String email) {
		Client c4updatee = AdminDAOImpl.db.map.get(cliendid);
		if (c4updatee != null) {
			c4updatee.setClientemail(email);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Boolean updateAddress(Integer cliendid, String address) {
		Client c4updatea = AdminDAOImpl.db.map.get(cliendid);
		if (c4updatea != null) {
			c4updatea.setClientadd(address);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Boolean updatePhoneno(Integer cliendid, String newno) {
		Client c4updatep = AdminDAOImpl.db.map.get(cliendid);
		if (c4updatep != null) {
			c4updatep.setClientphoneno(newno);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public Contract createNewContract(Integer clientid, Contract contract) {
		Client getclient = AdminDAOImpl.db.map.get(clientid);
		if (getclient != null) {
			AdminDAOImpl.cc.map4contract.put(contract.getContractid(), contract);
			getclient.setContract(contract);
			Haulier haul=new Haulier();
			haul.setTid("Transport4contractid:"+contract.getContractid());
			haul.setClientid(clientid);
			haul.setProductid(contract.getRequiredproductid());
			haul.setClientaddress(getclient.getClientadd());
			Date date=new Date();                                                
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String strDate = formatter.format(date);
			haul.setOrderplacedondate(strDate);
			haulcontract.map4haul.put(haul.getTid(), haul);
			return contract;
		} else {
			return null;
		}
	}

	@Override
	public Contract modifyContractQuantity(Integer cliendid, Integer contractid, Integer newquantity, String password) {
		Client getclient = AdminDAOImpl.db.map.get(cliendid);
		if (getclient != null) {
			if (getclient.getClientpassword().equals(password)) {
				Contract contract2update = AdminDAOImpl.cc.map4contract.get(contractid);
				contract2update.setContractid(newquantity);
				Contract updatedcon = getclient.getContract();
				updatedcon.setRequiredquantity(newquantity);
				return updatedcon;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

}
