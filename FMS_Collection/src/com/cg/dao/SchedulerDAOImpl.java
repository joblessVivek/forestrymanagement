package com.cg.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.cg.beans.Haulier;
import com.cg.beans.Inventory;
import com.cg.repo.InventoryDatabase;

public class SchedulerDAOImpl implements SchedulerDAO {

	static public InventoryDatabase i=new InventoryDatabase();
	
	@Override
	public Boolean createInventoryProfile(Inventory inv) {
		i.map.put(inv.getProductid(), inv);
		return true;
	}

	@Override
	public Inventory searchInventory(String invid) {
		return i.map.get(invid);
	}

	@Override
	public Inventory deleteInventory(String invid) {
		return i.map.remove(invid);
	}

	@Override
	public Inventory updateInventory(String invid,Integer newquantity) {
		Inventory updateinv=i.map.get(invid);
		updateinv.setAvailableproductquantity(newquantity);
		return updateinv;
	}

	@Override
	public List<Inventory> getAllInventory() {
		Collection<Inventory> c=i.map.values();
		List<Inventory> al=new ArrayList<Inventory>(c);
		return al;
	}

	@Override
	public Haulier scheduleDelivery(String doo, String eta) {
		return null;
	}

}
