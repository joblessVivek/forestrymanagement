package com.cg.dao;

public interface HaulierDAO {

	// Schedule Delivery
	public Boolean scheduleTransport(Integer contractid , Integer extradays);

	// Update delivery date
	public void updateDeliveryDate();
}
